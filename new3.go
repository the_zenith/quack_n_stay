package main

import (
	"crypto/rand"
	"crypto/rsa"
	"crypto/sha256"
	"encoding/pem"
	"encoding/x509"
	"fmt"
	"math/big"
	"os"
)

type rsaKeyPair struct {
	privateKey *rsa.PrivateKey
	publicKey  *rsa.PublicKey
}

func generateKeyPair() (*rsaKeyPair, error) {
	privateKey, err := rsa.GenerateKey(rand.Reader, 2048)
	if err!= nil {
		return nil, err
	}

	return &rsaKeyPair{
		privateKey: privateKey,
		publicKey:  &privateKey.PublicKey,
	}, nil
}

func encrypt(plaintext []byte, publicKey *rsa.PublicKey) ([]byte, error) {
	label := []byte("OAEP")
	hash := sha256.New()
	ciphertext, err := rsa.EncryptOAEP(hash, rand.Reader, publicKey, plaintext, label)
	if err!= nil {
		return nil, err
	}
	return ciphertext, nil
}

func decrypt(ciphertext []byte, privateKey *rsa.PrivateKey) ([]byte, error) {
	label := []byte("OAEP")
	hash := sha256.New()
	plaintext, err := rsa.DecryptOAEP(hash, rand.Reader, privateKey, ciphertext, label)
	if err!= nil {
		return nil, err
	}
	return plaintext, nil
}

func performArithmeticOperation(ciphertext []byte, operation string, value int64) ([]byte, error) {
	n := new(big.Int).SetBytes(ciphertext)
	switch operation {
	case "add":
		n.Add(n, big.NewInt(value))
	case "subtract":
		n.Sub(n, big.NewInt(value))
	case "multiply":
		n.Mul(n, big.NewInt(value))
	case "divide":
		n.Div(n, big.NewInt(value))
	default:
		return nil, fmt.Errorf("invalid operation")
	}
	return n.Bytes(), nil
}

func main() {
	keyPair, err := generateKeyPair()
	if err!= nil {
		fmt.Println(err)
		return
	}

	// Save the private key to a file
	privateKeyBytes := pem.EncodeToMemory(&pem.Block{
		Type:  "RSA PRIVATE KEY",
		Bytes: x509.MarshalPKCS1PrivateKey(keyPair.privateKey),
	})
	privateKeyFile, err := os.Create("private_key.pem")
	if err!= nil {
		fmt.Println(err)
		return
	}
	_, err = privateKeyFile.Write(privateKeyBytes)
	if err!= nil {
		fmt.Println(err)
		return
	}
	privateKeyFile.Close()

	// Save the public key to a file
	publicKeyBytes := pem.EncodeToMemory(&pem.Block{
		Type:  "RSA PUBLIC KEY",
		Bytes: x509.MarshalPKIXPublicKey(keyPair.publicKey),
	})
	publicKeyFile, err := os.Create("public_key.pem")
	if err!= nil {
		fmt.Println(err)
		return
	}
	_, err = publicKeyFile.Write(publicKeyBytes)
	if err!= nil {
		fmt.Println(err)
		return
	}
	publicKeyFile.Close()

	fmt.Println("Enter a value to encrypt:")
	var input string
	fmt.Scanln(&input)
	plaintext := []byte(input)

	ciphertext, err := encrypt(plaintext, keyPair.publicKey)
	if err!= nil {
		fmt.Println(err)
		return
	}

	fmt.Println("Enter an arithmetic operation (add, subtract, multiply, divide):")
	var operation string
	fmt.Scanln(&operation)

	fmt.Println("Enter a value for the operation:")
	var value int64
	fmt.Scanln(&value)

	encryptedResult, err := performArithmeticOperation(ciphertext, operation, value)
	if err!= nil {
		fmt.Println(err)
		return
	}

	decryptedResult, err := decrypt(encryptedResult, keyPair.privateKey)
	if err!= nil {
		fmt.Println(err)
		return
	}

	fmt.Println("Decrypted result:", string(decryptedResult))
}
